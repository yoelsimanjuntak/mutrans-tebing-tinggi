package id.appjekneo.driver.utils.api.service;

import id.appjekneo.driver.json.AcceptRequestJson;
import id.appjekneo.driver.json.AcceptResponseJson;
import id.appjekneo.driver.json.BankResponseJson;
import id.appjekneo.driver.json.ChangePassRequestJson;
import id.appjekneo.driver.json.GetOnRequestJson;
import id.appjekneo.driver.json.JobResponseJson;
import id.appjekneo.driver.json.UpdateLocationRequestJson;
import id.appjekneo.driver.json.AllTransResponseJson;
import id.appjekneo.driver.json.DetailRequestJson;
import id.appjekneo.driver.json.DetailTransResponseJson;
import id.appjekneo.driver.json.EditKendaraanRequestJson;
import id.appjekneo.driver.json.EditprofileRequestJson;
import id.appjekneo.driver.json.GetHomeRequestJson;
import id.appjekneo.driver.json.GetHomeResponseJson;
import id.appjekneo.driver.json.LoginRequestJson;
import id.appjekneo.driver.json.LoginResponseJson;
import id.appjekneo.driver.json.PrivacyRequestJson;
import id.appjekneo.driver.json.PrivacyResponseJson;
import id.appjekneo.driver.json.RegisterRequestJson;
import id.appjekneo.driver.json.RegisterResponseJson;
import id.appjekneo.driver.json.ResponseJson;
import id.appjekneo.driver.json.TopupRequestJson;
import id.appjekneo.driver.json.TopupResponseJson;
import id.appjekneo.driver.json.VerifyRequestJson;
import id.appjekneo.driver.json.WalletRequestJson;
import id.appjekneo.driver.json.WalletResponseJson;
import id.appjekneo.driver.json.WithdrawRequestJson;
import id.appjekneo.driver.json.WithdrawResponseJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface DriverService {

    @POST("driver/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("driver/update_location")
    Call<ResponseJson> updatelocation(@Body UpdateLocationRequestJson param);

    @POST("driver/syncronizing_account")
    Call<GetHomeResponseJson> home(@Body GetHomeRequestJson param);

    @POST("driver/logout")
    Call<GetHomeResponseJson> logout(@Body GetHomeRequestJson param);

    @POST("driver/turning_on")
    Call<ResponseJson> turnon(@Body GetOnRequestJson param);

    @POST("driver/accept")
    Call<AcceptResponseJson> accept(@Body AcceptRequestJson param);

    @POST("driver/start")
    Call<AcceptResponseJson> startrequest(@Body AcceptRequestJson param);

    @POST("driver/finish")
    Call<AcceptResponseJson> finishrequest(@Body AcceptRequestJson param);

    @POST("driver/edit_profile")
    Call<LoginResponseJson> editProfile(@Body EditprofileRequestJson param);

    @POST("driver/edit_kendaraan")
    Call<LoginResponseJson> editKendaraan(@Body EditKendaraanRequestJson param);

    @POST("driver/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

    @POST("driver/history_progress")
    Call<AllTransResponseJson> history(@Body DetailRequestJson param);

    @POST("driver/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("driver/register_driver")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @POST("pelanggan/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("driver/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);

    @POST("driver/job")
    Call<JobResponseJson> job();


    @POST("pelanggan/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("pelanggan/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("driver/withdraw")
    Call<WithdrawResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("pelanggan/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("driver/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("driver/verifycode")
    Call<ResponseJson> verifycode(@Body VerifyRequestJson param);


}
