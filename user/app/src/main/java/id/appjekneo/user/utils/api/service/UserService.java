package id.appjekneo.user.utils.api.service;

import id.appjekneo.user.json.AllMerchantByNearResponseJson;
import id.appjekneo.user.json.AllMerchantbyCatRequestJson;
import id.appjekneo.user.json.AllTransResponseJson;
import id.appjekneo.user.json.BankResponseJson;
import id.appjekneo.user.json.BeritaDetailRequestJson;
import id.appjekneo.user.json.BeritaDetailResponseJson;
import id.appjekneo.user.json.ChangePassRequestJson;
import id.appjekneo.user.json.DetailRequestJson;
import id.appjekneo.user.json.EditprofileRequestJson;
import id.appjekneo.user.json.GetAllMerchantbyCatRequestJson;
import id.appjekneo.user.json.GetFiturResponseJson;
import id.appjekneo.user.json.GetHomeRequestJson;
import id.appjekneo.user.json.GetHomeResponseJson;
import id.appjekneo.user.json.GetMerchantbyCatRequestJson;
import id.appjekneo.user.json.LoginRequestJson;
import id.appjekneo.user.json.LoginResponseJson;
import id.appjekneo.user.json.MerchantByCatResponseJson;
import id.appjekneo.user.json.MerchantByIdResponseJson;
import id.appjekneo.user.json.MerchantByNearResponseJson;
import id.appjekneo.user.json.MerchantbyIdRequestJson;
import id.appjekneo.user.json.PrivacyRequestJson;
import id.appjekneo.user.json.PrivacyResponseJson;
import id.appjekneo.user.json.PromoRequestJson;
import id.appjekneo.user.json.PromoResponseJson;
import id.appjekneo.user.json.RateRequestJson;
import id.appjekneo.user.json.RateResponseJson;
import id.appjekneo.user.json.RegisterRequestJson;
import id.appjekneo.user.json.RegisterResponseJson;
import id.appjekneo.user.json.ResponseJson;
import id.appjekneo.user.json.SearchMerchantbyCatRequestJson;
import id.appjekneo.user.json.TopupRequestJson;
import id.appjekneo.user.json.TopupResponseJson;
import id.appjekneo.user.json.WalletRequestJson;
import id.appjekneo.user.json.WalletResponseJson;
import id.appjekneo.user.json.WithdrawRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface UserService {

    @POST("pelanggan/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("pelanggan/kodepromo")
    Call<PromoResponseJson> promocode(@Body PromoRequestJson param);

    @POST("pelanggan/listkodepromo")
    Call<PromoResponseJson> listpromocode(@Body PromoRequestJson param);

    @POST("pelanggan/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("pelanggan/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

    @POST("pelanggan/register_user")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @GET("pelanggan/detail_fitur")
    Call<GetFiturResponseJson> getFitur();

    @POST("pelanggan/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("pelanggan/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("pelanggan/home")
    Call<GetHomeResponseJson> home(@Body GetHomeRequestJson param);

    @POST("pelanggan/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("pelanggan/withdraw")
    Call<ResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("pelanggan/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("pelanggan/rate_driver")
    Call<RateResponseJson> rateDriver(@Body RateRequestJson param);

    @POST("pelanggan/edit_profile")
    Call<RegisterResponseJson> editProfile(@Body EditprofileRequestJson param);

    @POST("pelanggan/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("pelanggan/history_progress")
    Call<AllTransResponseJson> history(@Body DetailRequestJson param);

    @POST("pelanggan/detail_berita")
    Call<BeritaDetailResponseJson> beritadetail(@Body BeritaDetailRequestJson param);

    @POST("pelanggan/all_berita")
    Call<BeritaDetailResponseJson> allberita(@Body BeritaDetailRequestJson param);

    @POST("pelanggan/merchantbykategoripromo")
    Call<MerchantByCatResponseJson> getmerchanbycat(@Body GetMerchantbyCatRequestJson param);

    @POST("pelanggan/merchantbykategori")
    Call<MerchantByNearResponseJson> getmerchanbynear(@Body GetMerchantbyCatRequestJson param);

    @POST("pelanggan/allmerchantbykategori")
    Call<AllMerchantByNearResponseJson> getallmerchanbynear(@Body GetAllMerchantbyCatRequestJson param);

    @POST("pelanggan/itembykategori")
    Call<MerchantByIdResponseJson> getitembycat(@Body GetAllMerchantbyCatRequestJson param);

    @POST("pelanggan/searchmerchant")
    Call<AllMerchantByNearResponseJson> searchmerchant(@Body SearchMerchantbyCatRequestJson param);

    @POST("pelanggan/allmerchant")
    Call<AllMerchantByNearResponseJson> allmerchant(@Body AllMerchantbyCatRequestJson param);

    @POST("pelanggan/merchantbyid")
    Call<MerchantByIdResponseJson> merchantbyid(@Body MerchantbyIdRequestJson param);


}
