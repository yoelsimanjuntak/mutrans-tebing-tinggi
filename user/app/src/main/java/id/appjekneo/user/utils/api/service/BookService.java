package id.appjekneo.user.utils.api.service;

import id.appjekneo.user.json.CheckStatusTransaksiRequest;
import id.appjekneo.user.json.CheckStatusTransaksiResponse;
import id.appjekneo.user.json.DetailRequestJson;
import id.appjekneo.user.json.DetailTransResponseJson;
import id.appjekneo.user.json.GetNearRideCarRequestJson;
import id.appjekneo.user.json.GetNearRideCarResponseJson;
import id.appjekneo.user.json.ItemRequestJson;
import id.appjekneo.user.json.LokasiDriverRequest;
import id.appjekneo.user.json.LokasiDriverResponse;
import id.appjekneo.user.json.RideCarRequestJson;
import id.appjekneo.user.json.RideCarResponseJson;
import id.appjekneo.user.json.SendRequestJson;
import id.appjekneo.user.json.SendResponseJson;
import id.appjekneo.user.json.fcm.CancelBookRequestJson;
import id.appjekneo.user.json.fcm.CancelBookResponseJson;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/17/2019.
 */

public interface BookService {

    @POST("pelanggan/list_ride")
    Call<GetNearRideCarResponseJson> getNearRide(@Body GetNearRideCarRequestJson param);

    @POST("pelanggan/list_car")
    Call<GetNearRideCarResponseJson> getNearCar(@Body GetNearRideCarRequestJson param);

    @POST("pelanggan/request_transaksi")
    Call<RideCarResponseJson> requestTransaksi(@Body RideCarRequestJson param);

    @POST("pelanggan/inserttransaksimerchant")
    Call<RideCarResponseJson> requestTransaksiMerchant(@Body ItemRequestJson param);

    @POST("pelanggan/request_transaksi_send")
    Call<SendResponseJson> requestTransaksisend(@Body SendRequestJson param);

    @POST("pelanggan/check_status_transaksi")
    Call<CheckStatusTransaksiResponse> checkStatusTransaksi(@Body CheckStatusTransaksiRequest param);

    @POST("pelanggan/user_cancel")
    Call<CancelBookResponseJson> cancelOrder(@Body CancelBookRequestJson param);

    @POST("pelanggan/liat_lokasi_driver")
    Call<LokasiDriverResponse> liatLokasiDriver(@Body LokasiDriverRequest param);

    @POST("pelanggan/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);


}
