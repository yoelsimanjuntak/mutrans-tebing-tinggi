package id.appjekneo.mitra.utils.api.service;

import id.appjekneo.mitra.json.ActivekatRequestJson;
import id.appjekneo.mitra.json.AddEditItemRequestJson;
import id.appjekneo.mitra.json.AddEditKategoriRequestJson;
import id.appjekneo.mitra.json.BankResponseJson;
import id.appjekneo.mitra.json.ChangePassRequestJson;
import id.appjekneo.mitra.json.DetailRequestJson;
import id.appjekneo.mitra.json.DetailTransResponseJson;
import id.appjekneo.mitra.json.EditMerchantRequestJson;
import id.appjekneo.mitra.json.EditProfileRequestJson;
import id.appjekneo.mitra.json.GetFiturResponseJson;
import id.appjekneo.mitra.json.GetOnRequestJson;
import id.appjekneo.mitra.json.HistoryRequestJson;
import id.appjekneo.mitra.json.HistoryResponseJson;
import id.appjekneo.mitra.json.HomeRequestJson;
import id.appjekneo.mitra.json.HomeResponseJson;
import id.appjekneo.mitra.json.ItemRequestJson;
import id.appjekneo.mitra.json.ItemResponseJson;
import id.appjekneo.mitra.json.KategoriRequestJson;
import id.appjekneo.mitra.json.KategoriResponseJson;
import id.appjekneo.mitra.json.LoginRequestJson;
import id.appjekneo.mitra.json.LoginResponseJson;
import id.appjekneo.mitra.json.PrivacyRequestJson;
import id.appjekneo.mitra.json.PrivacyResponseJson;
import id.appjekneo.mitra.json.RegisterRequestJson;
import id.appjekneo.mitra.json.RegisterResponseJson;
import id.appjekneo.mitra.json.ResponseJson;
import id.appjekneo.mitra.json.TopupRequestJson;
import id.appjekneo.mitra.json.TopupResponseJson;
import id.appjekneo.mitra.json.WalletRequestJson;
import id.appjekneo.mitra.json.WalletResponseJson;
import id.appjekneo.mitra.json.WithdrawRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface MerchantService {

    @GET("merchant/kategorimerchant")
    Call<GetFiturResponseJson> getFitur();

    @POST("pelanggan/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("merchant/kategorimerchantbyfitur")
    Call<GetFiturResponseJson> getKategori(@Body HistoryRequestJson param);

    @POST("merchant/onoff")
    Call<ResponseJson> turnon(@Body GetOnRequestJson param);

    @POST("merchant/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("merchant/register_merchant")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @POST("merchant/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("pelanggan/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("merchant/edit_profile")
    Call<LoginResponseJson> editprofile(@Body EditProfileRequestJson param);

    @POST("merchant/edit_merchant")
    Call<LoginResponseJson> editmerchant(@Body EditMerchantRequestJson param);

    @POST("merchant/home")
    Call<HomeResponseJson> home(@Body HomeRequestJson param);

    @POST("merchant/history")
    Call<HistoryResponseJson> history(@Body HistoryRequestJson param);

    @POST("merchant/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);

    @POST("merchant/kategori")
    Call<KategoriResponseJson> kategori(@Body KategoriRequestJson param);

    @POST("merchant/item")
    Call<ItemResponseJson> itemlist(@Body ItemRequestJson param);

    @POST("merchant/active_kategori")
    Call<ResponseJson> activekategori(@Body ActivekatRequestJson param);

    @POST("merchant/active_item")
    Call<ResponseJson> activeitem(@Body ActivekatRequestJson param);

    @POST("merchant/add_kategori")
    Call<ResponseJson> addkategori(@Body AddEditKategoriRequestJson param);

    @POST("merchant/edit_kategori")
    Call<ResponseJson> editkategori(@Body AddEditKategoriRequestJson param);

    @POST("merchant/delete_kategori")
    Call<ResponseJson> deletekategori(@Body AddEditKategoriRequestJson param);

    @POST("merchant/add_item")
    Call<ResponseJson> additem(@Body AddEditItemRequestJson param);

    @POST("merchant/edit_item")
    Call<ResponseJson> edititem(@Body AddEditItemRequestJson param);

    @POST("merchant/delete_item")
    Call<ResponseJson> deleteitem(@Body AddEditItemRequestJson param);

    @POST("pelanggan/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("merchant/withdraw")
    Call<ResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("pelanggan/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("merchant/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("merchant/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

}
